# iBackup - Backup your activation tickets like never before.
Please note this is still a very W.I.P tool and may fail occasionally. Please take any bootlooping as your own fault.
<br>

## Getting started
Read [this](https://modiverse.co.uk/ibackup-tut/) page. I hate markdown and I'm not bothering with it, so endure my magnicifent HTML abilities.

## Credits
[Nathan](https://github.com/verygenericname/) for SSHRD.<br>
iOS Euphoria for being enough of a twat (due to charging to backup activation tickets) for me to make this tool.
<br>

## Licensing
I am not liable for any damages that have been caused to your device by this tool, and honestly I couldn't care less.<br>
You chose to use this. You cannot sue me thanks to the licensing that you agreed to by using this tool.<br>
This project is licensed under MIT. More can be found in the LICENSE file in the root of this repository.
