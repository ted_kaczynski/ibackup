#!/usr/bin/env bash
#* =========================================================================================================================================================
#* Licensed under the MIT license
#* Created by Genesis
#* Out of spite for iOS Euphoria.
#* Enjoy!
#*
#* https://mit-license.org/
#*
#* Copyright © 2023 Genesis
#* Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the 'Software'), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#* =========================================================================================================================================================

# make sure we are in the scripts dir
script_full_path=$(dirname "$0")
cd $script_full_path || exit 1

# check if macOS or Linux, then if the macOS version is compatible.
if [[ $(uname) == "Darwin" ]]; then
    echo '[*] Please note that this script only works on macOS Big Sur or higher.'
    echo '[*] If you are not on this version, please use OCLP or another tool to update.'
fi

# check if sshpass and iproxy installed
if command -v iproxy &>/dev/null; then
    echo >> /dev/null
else
    echo '[*] ERROR: iProxy is not installed. Installing it now.'
    # install sshpass or iproxy
    if command -v apt &>/dev/null; then
	sudo apt install libusbmuxd usbmuxd libimobiledevice
    elif command -v brew &>/dev/null; then
	brew install libusbmuxd usbmuxd libimobiledevice
    elif command -v pacman &>/dev/null; then
	sudo pacman -S libusbmuxd usbmuxd libimobiledevice
    elif command -v dnf &>/dev/null; then
	sudo dnf install libusbmuxd usbmuxd libimobiledevice
    elif command -v yum &>/dev/null; then
	sudo yum install libusbmuxd usbmuxd libimobiledevice
    else
	echo '[*] ERROR: Package manager not supported or not detected. Install libusbmuxd, usbmuxd, and libimobiledevice and retry.'
    fi
fi
if command -v sshpass &>/dev/null; then
    echo >> /dev/null
else
    echo '[*] ERROR: SSHPass is not installed. Installing it now.'
    # install sshpass or iproxy
    if command -v apt &>/dev/null; then
	sudo apt install sshpass
    elif command -v brew &>/dev/null; then
	brew install sshpass
    elif command -v pacman &>/dev/null; then
	sudo pacman -S sshpass
    elif command -v dnf &>/dev/null; then
	sudo dnf install sshpass
    elif command -v yum &>/dev/null; then
	sudo yum install sshpass
    else
	echo '[*] ERROR: Package manager not supported or not detected. Install sshpass and retry.'
    fi
fi

if [[ "$1" == "--backup" ]]; then
    #check if sshrd exists
    if [ -d ".rd/" ]; then
	echo '[*] SSHRD already exists. Pulling latest commits.'
	cd .rd/
	git pull
	cd ..
    else
	# clone sshrd script :trollogus:
	echo '[*] Cloning SSHRD. May take a while.'
	git clone https://github.com/verygenericname/SSHRD_Script.git --recursive
	mv SSHRD_Script/ .rd/
	chmod 777 .rd/sshrd.sh
    fi

    # And booting sshrd.
    cd .rd/
    read -p '[*] Enter the iOS version of your device: ' ver
    if [[ $(uname) == "Linux" ]]; then
	echo '[*] If your iOS version is above or equal to 16.1, SSHRD will not work.'
    fi
    sudo bash sshrd.sh $ver
    sudo bash sshrd.sh boot

    # set funcs, vars
    remote_cmd() {
	iproxy 2222 22 &>/dev/null &
	sshpass -p 'alpine' ssh -o StrictHostKeyChecking=no -p 2222 root@localhost "$@"
	killall iproxy
    }

    remote_scp() {
	sourceDir="$1"
	destinationDir="$2"
	remote_cmd "scp -r \"$sourceDir\" \"$destinationDir\""
    }

    baseDir="/mnt2/containers/Data/System"
    recordsDir=$(remote_cmd "find \"$baseDir\" -type d -name \"activation_records\" -print -quit")
    libraryRecordsDir="$recordsDir/.."
    actUUID1="${recordsDir##*/}"
    actUUID2="${actUUID1//\//}"
    commCenterPlist="/mnt2/wireless/Library/com.apple.commcenter.device_specific_nobackup.plist"
    fairplayDir="/mnt2/mobile/Library/FairPlay/"
    currentDate=$(date +%d/%m/%Y)

    # boot sshrd
    mkdir -p ./Tickets/
    mkdir -p ./Tickets/$currentDate/
    remote_cmd "mount_filesystems"
    mkdir -p ./Tickets/$currentDate/$actUUID2 # make it device specific.
    echo "[*] SUCCESS: Found activation ticket hash: $actUUID2"
    remote_scp $commCenterPlist ./Tickets/$currentDate/$actUUID2
    remote_scp $fairplayDir ./Tickets/$currentDate/$actUUID2
    remote_scp $libraryRecordsDir ./Tickets/$currentDate/$actUUID2
    echo '[*] SUCCESS: Your activation tickets and relevant files needed for activation are now in ./Tickets/'$currentDate'/'$actUUID2'. You may restore them (to the same device) by rerunning this script with the --restore <path> argument.'
    bash sshrd.sh reboot


elif [[ "$1" == "--restore" && -n "$2" ]]; then
    restorePath="$2"

    # verify path exists
    if [[ -d "$restorePath/" ]]; then
	echo "[*] Restoring tickets from $restorePath"
	#check if sshrd exists
	if [ -d ".rd/" ]; then
	    echo '[*] SSHRD already exists. Pulling latest commits.'
	    cd .rd/
	    git pull
	    cd ..
	else
	    # clone sshrd script :trollogus:
	    echo '[*] Cloning SSHRD. May take a while.'
	    git clone https://github.com/verygenericname/SSHRD_Script.git --recursive
	    mv SSHRD_Script/ .rd/
	    chmod 777 .rd/sshrd.sh
	fi

	# set funcs, vars
	remote_cmd() {
	    iproxy 2222 22 &>/dev/null &
	    sshpass -p 'alpine' ssh -o StrictHostKeyChecking=no -p 2222 root@localhost "$@"
	    killall iproxy
	    # ./"$dir"/sshpass -p 'alpine' ssh -o StrictHostKeyChecking=no -p 6413 root@localhost# "$@"
	}

	local_scp() {
	    localPath="$1"
	    destinationDirectory="$2"
	    iproxy 2222 22 &>/dev/null &
	    sshpass -p 'alpine' scp -o StrictHostKeyChecking=no -P 2222 "$localPath" "root@localhost:$destinationDirectory"
	    killall iproxy
	}

	baseDir="/mnt2/containers/Data/System"
	recordsDir=$(remote_cmd "find \"$baseDir\" -type d -name \"activation_records\" -print -quit")
	libraryRecordsDir="$recordsDir/.."
	actUUID1="${recordsDir##*/}"
	actUUID2="${actUUID1//\//}"
	commCenterPlist="/mnt2/wireless/Library/com.apple.commcenter.device_specific_nobackup.plist"
	fairplayDir="/mnt2/mobile/Library/FairPlay/"
	currentDate=$(date +%d/%m/%Y)

	# boot sshrd
	cd .rd/
	read -p '[*] Enter the iOS version of your device: ' ver
	if [[ $(uname) == "Linux" ]]; then
	    echo "[*] If your iOS version is above or equal to 16.1, SSHRD will not work."
	fi
	sudo bash sshrd.sh $ver
	sudo bash sshrd.sh boot

	# copy back the dirs.
	remote_cmd "mount_filesystems"
	remote_cmd "rm -f /mnt2/wireless/Library/com.apple.commcenter.device_specific_nobackup.plist"
	remote_cmd "rm -rf /mnt2/mobile/Library/FairPlay/"
	remote_cmd "rm -rf $baseDir/$actUUID2/Library/"
	local_scp "$restorePath/com.apple.commcenter.device_specific_nobackup.plist" /mnt2/wireless/Library/
	local_scp "$restorePath/FairPlay/" /mnt2/mobile/Library/
	local_scp "$restorePath/Library/" $baseDir/$actUUID2/
	echo '[*] SUCCESS: Your activation tickets and relevant files needed for activation are now back in your iDevice.'
    else
	echo "[*] ERROR: The specified path does not exist or is not a directory: $restorePath"
    fi
else
    echo "[*] ERROR: Invalid arguments. Please refer to the README document in this directory: $script_full_path/"
fi
if [[ "$1" == "--clean" ]]; then
    cd .rd/
    bash sshrd.sh clean
    cd ..
fi
